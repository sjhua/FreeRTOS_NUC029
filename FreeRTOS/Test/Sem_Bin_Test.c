/*
    FreeRTOS V6.0.5 - Copyright (C) 2010 Real Time Engineers Ltd.

    ***************************************************************************
    *                                                                         *
    * If you are:                                                             *
    *                                                                         *
    *    + New to FreeRTOS,                                                   *
    *    + Wanting to learn FreeRTOS or multitasking in general quickly       *
    *    + Looking for basic training,                                        *
    *    + Wanting to improve your FreeRTOS skills and productivity           *
    *                                                                         *
    * then take a look at the FreeRTOS eBook                                  *
    *                                                                         *
    *        "Using the FreeRTOS Real Time Kernel - a Practical Guide"        *
    *                  http://www.FreeRTOS.org/Documentation                  *
    *                                                                         *
    * A pdf reference manual is also available.  Both are usually delivered   *
    * to your inbox within 20 minutes to two hours when purchased between 8am *
    * and 8pm GMT (although please allow up to 24 hours in case of            *
    * exceptional circumstances).  Thank you for your support!                *
    *                                                                         *
    ***************************************************************************

    This file is part of the FreeRTOS distribution.

    FreeRTOS is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License (version 2) as published by the
    Free Software Foundation AND MODIFIED BY the FreeRTOS exception.
    ***NOTE*** The exception to the GPL is included to allow you to distribute
    a combined work that includes FreeRTOS without being obliged to provide the
    source code for proprietary components outside of the FreeRTOS kernel.
    FreeRTOS is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details. You should have received a copy of the GNU General Public 
    License and the FreeRTOS license exception along with FreeRTOS; if not it 
    can be viewed here: http://www.freertos.org/a00114.html and also obtained 
    by writing to Richard Barry, contact details for whom are available on the
    FreeRTOS WEB site.

    1 tab == 4 spaces!

    http://www.FreeRTOS.org - Documentation, latest information, license and
    contact details.

    http://www.SafeRTOS.com - A version that is certified for use in safety
    critical systems.

    http://www.OpenRTOS.com - Commercial support, development, porting,
    licensing and training services.
*/

#include <stdio.h>

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

#include "FreeRTOS_Test.h"

#define SEM_BIN_TEST_STACK_SIZE		configMINIMAL_STACK_SIZE	
#define SEM_BIN_TEST_PRIORITY		( tskIDLE_PRIORITY )

xSemaphoreHandle xSemaphore;
signed long count = 0;

void Sem_Bin_Task1( void *pvParameters );
void Sem_Bin_Task2( void *pvParameters );


void vStartSemBinTasks( void )
{				
	/* Create semaphore. */
	vSemaphoreCreateBinary( xSemaphore );

	xTaskCreate( Sem_Bin_Task1, ( signed char * ) "SemB_Task1", SEM_BIN_TEST_STACK_SIZE, ( void * ) NULL, SEM_BIN_TEST_PRIORITY, ( xTaskHandle * ) NULL );
	xTaskCreate( Sem_Bin_Task2, ( signed char * ) "SemB_Task2", SEM_BIN_TEST_STACK_SIZE, ( void * ) NULL, ( SEM_BIN_TEST_PRIORITY + 1 ), ( xTaskHandle * ) NULL );
}

void Sem_Bin_Task2( void *pvParameters )
{
	if( xSemaphoreTake( xSemaphore, ( portTickType ) 0 ) == pdPASS )
	{
		printf( "Task2 take Semaphore\n" );
	}

	for( ; ; )
	{
		/* Try to release the Mutex. */
		if(	xSemaphoreGive( xSemaphore ) == pdPASS )
		{
			printf( "Task2 release Semaphore\n" );
			count++;	
		}
		vTaskDelay(1000);
	}
}

void Sem_Bin_Task1( void *pvParameters )
{
	for( ; ; )
	{
		/* Try to obtain the Mutex. */
		if( xSemaphoreTake( xSemaphore, ( portTickType ) 0 ) == pdPASS )
		{
			printf( "Task1 take Semaphore\n" );
			count--;
		}
		if( count < 0 )
		{
			printf( "Semaphore error\n" );
			break;
		}
	}
}























































































































