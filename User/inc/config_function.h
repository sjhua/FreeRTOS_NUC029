#ifndef __CONFIG_FUNCTION_H__
#define	__CONFIG_FUNCTION_H__

/////////////////////////////////////////////////////////////
//PCBA型号
#define		BOARD_STM32_V4			1		//STM32F103 开发板
#define		BOARD_TK035F5589_DEMO	2		//TK035F5589 TFT 演示DEMO板
#define		BOARD_C75P291_190613	3		//ZHJCR ALS MediaLight Panel V1.0
#define		BOARD_C75P327_191225	4		//生物探照灯

#define		BOARD_NAME				BOARD_C75P327_191225

#define		CFG_AT_EEPROM_24CXX		1
#define		CFG_AT_FLASH_W25QXX		2
#define		CFG_AT_MCU_LD_ROM		3
#define		CFG_AT_MCU_AP_ROM		4

#define		CFG_AT_WHERE			CFG_AT_FLASH_W25QXX

#define		UART_NULL				0
#define		UART_SW_MODE			1

#define		SW_UART_EN				UART_NULL

//项目功能模块选择
#define		HW_UART0_EN				1
#define		HW_UART1_EN				0
#define		IR_DECODE_EN			0
#define		IR_ENCODE_EN			0
#define		ADC_CONVERT_EN			0
#define		EEPROM_RD_WT_EN			0
#define		FLASH_RD_WT_EN			0
#define		FLASH_W25Q_EN			0
#define		FLASH_SECTOR_RD_WT_EN	0
#define		USER_NUMBER_EN			0
#define		LOADER_ADJ_EN			0
#define		I2CS_EN					0
#define		SPIM_EN					0
#define		KEY_SCAN_EN				0
#define		CHARGE_EN				0
#define		SYS_IDLE_EN				0

#define		HU_RXTX_EN				0

#define		RFID_SKY1311S_EN		0

#define		IR_TX_EN				0
#define		IR_RX_EN				0

#define		LINKLIST_EN				0

#define		FIFO_QUEUE_EN			0

#define		TP_GT811_EN				0
#define		TP_TSC2046_EN			0

#define		TP_FT6206_EN			0

#define		LCD_RA8875_EN			0
#define		LCD_SPFD5420_EN			0
#define		LCD_R61529_EN			0
#define		LCD_ST7789S_EN			0

#define		OS_FREERTOS_EN			1

#if 1
#define		hu_printf		printf
//#define		hu_memset		memset
//#define		hu_memcpy		memcpy
#else
#define		hu_printf(...)                  {}
#endif

#endif
